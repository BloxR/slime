const config = require('../conf.json');
const discord = require('discord.js');
const fs = require('fs');
let cmdlist = fs.readdirSync('./commands');
    const cmdlistarray = []
    cmdlist.forEach(f => {
        cmdlistarray.push(f.slice(0,-3));
    });
module.exports.run = async (client, message, args) => {
    if (!args[0]) {
        let helpembed = new discord.RichEmbed()
        .setColor('#FFFF')
        .setAuthor('RandomBot',message.guild.iconURL)
        .setTitle('Commands for this bot')
        .setDescription(`__Commands:__\n\n${cmdlistarray.join(" **|** ")}`)
        .setFooter("Commands")
        message.author.send(helpembed) // Send Message in DM
            .then(()=>{
                // Sent
                message.channel.send(`${message.author.toString()} Please check your direct messages`).then(m => m.delete(7000)).catch(_=>{});
            })
            .catch((e)=>{
                // Failed to Send
                message.channel.send(`${message.author.toString()} We couldn't send you a DM.`).then(m => m.delete(7000)).catch(_=>{});
            });
    }
    if (args[0]){
        let selectedcommand = args[0];  
        if (!client.commands.has(selectedcommand)) {
            return message.channel.send(`${message.author.toString()} This command does not exist do ${config.prefix}help to see a full list of commands (aliases do not work for the help command)`).then(m => m.delete(7000)).catch(_=>{});
        }
        if (client.commands.has(selectedcommand)) {
            selectedcommand = client.commands.get(selectedcommand);
            const botembed = new discord.RichEmbed()
            .setColor('#FFFF')
            .setAuthor('RandomBot',message.guild.iconURL)
            .setDescription(`__Current prefix:__ ${config.prefix}\n\n**Command:** ${selectedcommand.config.name}\n**Aliases:** ${selectedcommand.config.aliases.join(" **|** ") || "No aliases"}`);
            message.channel.send(botembed);
        }
    }
}
module.exports.config = {
    name: 'help',
    aliases: ["h","commands","cmds","info"],
}

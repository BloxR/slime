const config = require('./conf.json');
const discord = require('discord.js');
const fs = require('fs');

const client = new discord.Client({
    disableEveryone: true
});

client.commands = new discord.Collection();
client.aliases = new discord.Collection();

fs.readdir("./commands", (err, files)=>{
    if (err) return console.log (`[LOGS] Error: ${err}`);
    let jsfile = files.filter(f => f.split(".")[0]);
    if (jsfile.length <= 0){
       return console.log("[LOGS] Commands not found. Bot loaded with no commands!");
    }
    jsfile.forEach((f) => {
        let pull = require(`./commands/${f}`);
        client.commands.set(pull.config.name, pull);
        pull.config.aliases.forEach(alias =>{
            client.aliases.set(alias, pull.config.name);
        });
        console.log(`[LOGS] Command Loaded: ${f}`);
    });
});

client.on('ready',()=>{
    console.log('Online');
    client.user.setActivity(`Prefix: ${config.prefix}`);
});

client.on('message',async message =>{
    if (message.author.bot) return;
    if (message.channel.type === "dm") return;
    if (message.content.indexOf(config.prefix)) return;
    const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();
    const commandFile = client.commands.get(client.aliases.get(command) || command);
    if (commandFile){
        message.delete().catch(_=>{});
        commandFile.run(client, message, args);
    }
    if (!commandFile){
        message.delete().catch(_=>{});
        message.channel.send(`${message.author.toString()} Command not found`).then(m => m.delete(7000)).catch(_=>{});
    }
});

client.login(config.token);